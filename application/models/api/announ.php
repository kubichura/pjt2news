<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Announ extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_all_announ(){
        $this->db->order_by('announ_id', 'DESC');

        $query = $this->db->get('announcement')->result();

        if ($query != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function get_detail_announ($id){
        $this->db->where('announ_id', $id);

        $query = $this->db->get('announcement')->result();

        if ($query != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }
}

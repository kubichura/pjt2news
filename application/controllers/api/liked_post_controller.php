<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Liked_Post_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('liked_post');
  }

  public function getLikePost(){
      $id = $this->input->post('id');

      $respone = $this->liked_post->get_like_post($id);

      echo json_encode($respone);
  }

  public function getCountLikePost(){
      $id = $this->input->post('id');

      $respone = $this->liked_post->get_count_like_post($id);

      echo json_encode($respone);
  }

  public function getLikePostByUser(){
      $id = $this->input->post('id');
      $nik = $this->input->post('nik');

      $respone = $this->liked_post->get_like_post_by_user($id, $nik);

      echo json_encode($respone);
  }

  public function saveLikePost(){
      $id = $this->input->post('id');
      $nik = $this->input->post('nik');

      $respone = $this->liked_post->save_liked_post_by_user($id, $nik);

      echo json_encode($respone);
  }

  public function removeLikePost(){
      $id = $this->input->post('id');
      $nik = $this->input->post('nik');

      $respone = $this->liked_post->remove_liked_post_by_user($id, $nik);

      echo json_encode($respone);
  }

}
<!doctype html>
<html>
    <head>
        <!-- jQuery 3.2.1 -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/> -->
        <script type="text/javascript">
            $(function () {
                $('#user_birthday').datepicker({
                    viewMode: 'years',
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                });
            });
        </script>
    </head>
    <body>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Users
                <small><?php echo $button; ?></small>
                </h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>users">Users</a></li>
                <li class="active"><?php echo $button; ?></li>
                <!--
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
                -->
                </ol>
            </section>

            <!-- Main content -->
	        <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form User <?php echo $button; ?></h3>
                    </div> <!-- box-header -->
                

                    <div class="box-body">
                        <!-- <h2 style="margin-top:0px">Users <?php echo $button ?></h2> -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label for="varchar">Nik <?php echo form_error('user_nik') ?></label>
                                <input type="text" class="form-control" name="user_nik" id="user_nik" placeholder="Nik" value="<?php echo $user_nik; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Name <?php echo form_error('user_name') ?></label>
                                <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Name" value="<?php echo $user_name; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Email <?php echo form_error('user_email') ?></label>
                                <input type="text" class="form-control" name="user_email" id="user_email" placeholder="Email" value="<?php echo $user_email; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="date">Birthday <?php echo form_error('user_birthday') ?></label>
                                <!-- <input type="text" class="form-control" name="user_birthday" id="user_birthday" placeholder="User Birthday" value="<?php echo $user_birthday; ?>" /> -->
                            
                                <div class='input-group date' id='user_birthday'>
                                    <input type='text' name="user_birthday" id="user_birthday" class="form-control" placeholder="YYYY-MM-DD" value="<?php echo $user_birthday; ?>" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="varchar">Gender <?php echo form_error('gender') ?></label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="L" <?php if($gender=="L") echo 'selected="selected"'; ?> >Laki-Laki</option> 
                                    <option value="P" <?php if($gender=="P") echo 'selected="selected"'; ?> >Perempuan</option>    
                                </select>
                                <!-- <input type="text" class="form-control" name="gender" id="gender" placeholder="Gender" value="<?php echo $gender; ?>" /> -->
                            </div>
                            <!-- <div class="form-group">
                                <label for="path_profile">Path Profile <?php echo form_error('path_profile') ?></label>
                                <textarea class="form-control" rows="3" name="path_profile" id="path_profile" placeholder="Path Profile"><?php echo $path_profile; ?></textarea>
                            </div> -->
                            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" /> 
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button> 
                            <a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        
                    </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
        </div> <!-- wrapper -->
    </body>
</html>
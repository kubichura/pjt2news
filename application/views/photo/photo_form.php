<!doctype html>
<html>
    <head>
    </head>
    <body>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Photo
                <small><?php echo $button; ?></small>
                </h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>photo">Photo</a></li>
                <li class="active"><?php echo $button; ?></li>
                <!--
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
                -->
                </ol>
            </section>

            <!-- Main content -->
	        <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Photo <?php echo $button; ?></h3>
                    </div> <!-- box-header -->
                    <div class="box-body">
                        <form action="<?php echo $action; ?>" enctype="multipart/form-data" method="post">

                            <input type="hidden" name="photo_thumb" value="<?php echo $photo_path; ?>">
                            <div class="form-group custom-file">
                                <label for="photo_path" class="custom-file-label">Photo Path <?php echo form_error('photo_path') ?></label>
                                <input type="file" class="form-control custom-file-input"  name="photo_path" id="photo_path" placeholder="Photo Path" />
                            </div>
                            <div class="form-group">
                                <label for="photo_caption">Photo Caption <?php echo form_error('photo_caption') ?></label>
                                <textarea class="form-control" rows="3" name="photo_caption" id="photo_caption" placeholder="Photo Caption"><?php echo $photo_caption; ?></textarea>
                            </div>
                            <!-- <div class="form-group">
                                <label for="date">Photo Date <?php echo form_error('photo_date') ?></label>
                                <input type="text" class="form-control" name="photo_date" id="photo_date" placeholder="Photo Date" value="<?php echo $photo_date; ?>" />
                            </div> -->
                            <input type="hidden" name="photo_id" value="<?php echo $photo_id; ?>" /> 
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i><?php echo $button ?></button> 
                            <a href="<?php echo site_url('photo') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
                

        </div> <!-- wrapper -->
    </body>
</html>